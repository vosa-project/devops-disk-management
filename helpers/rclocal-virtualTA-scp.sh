#!/bin/bash

#Author: Katrin Loodus
#Modified by Oliver Tiks
#Last modify date: 10.05.2017

#Description: This script is used for copying back rc.local and virtualTA.sh files to lab desktop after they have been modified and fetched from the repository. 

#Important! Run this script AFTER you have synced the directory with git. 

#seccopies updated rc.local and virtualTA.sh scripts to template desktop
find /root/devops-disk-management/desktop -mtime 0 -type f \( -name "rc.local" -o -name "*virtualTA.sh" \) -exec scp {} root@labdesktop:/root \;

#seccopies updated rc.local and cleanup.sh scripts to template server
find /root/devops-disk-management/server -mtime 0 -type f \( -name "rc.local" -o -name "cleanup.sh" \) -exec scp {} root@labserver:/var/tmp \;
