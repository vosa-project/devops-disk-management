#!/bin/bash

# description "Ordered list of linear checks for DEVOPS Disk Management lab"

#/root/devops-disk-management/router/lab_diskmgt/step_xxx || true
/root/devops-disk-management/router/lab_diskmgt/step_sshconnection.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_sudoprivileges.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_primpart.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_extpart.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_logicpart1.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_logicpart2.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_swappiness.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_swaparea.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_filesystem1.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_filesystem2.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_swapon.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_manualmount.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_swapoff.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_manualunmount.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_autoswapon.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_automount1.sh || true
/root/devops-disk-management/router/lab_diskmgt/step_automount2.sh || true

