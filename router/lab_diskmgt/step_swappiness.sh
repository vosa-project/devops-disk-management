#!/bin/bash
# Script for checking DEVOPS Disk Management lab objectives
# Objective name - Optimize swap usage

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks
#
# Date - 12.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# SWAPPINESS

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/swappiness"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - diskmgt server
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=swappiness

}

# User interaction: Decrease swap usage from the default 60 to 10 permanently

SWAPPINESS () {

	while true
	do

   	# Check if user has set swappiness parameter to 10 permanently 
    	ssh root@$IP_to_SSH 'cat /proc/sys/vm/swappiness | grep -x 10 && grep vm.swappiness /etc/sysctl.conf  | grep -v "#" | grep -w 10'  

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nSwappiness has been decreased to 10 permanently! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Swappiness parameter is not configured to be 10 permanently! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

SWAPPINESS

exit 0

