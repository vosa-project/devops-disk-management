#!/bin/bash
# Script that creates virtualTA desktop icon
if test -f "/home/student/.config/autostart/virtualta.desktop"
then
touch /tmp/virtualtafileexists
rm "/home/student/.config/autostart/virtualta.desktop" && touch /tmp/virtualtafiledeleted
fi

if [ $(dmidecode -s system-serial-number) != 'System Serial' ]
then
cat > /home/student/.config/autostart/virtualta.desktop <<EOF
[Desktop Entry]
Type=Application
Exec=chromium-browser --app=https://i-tee.itcollege.ee:8433/lab/$(dmidecode -s system-serial-number) --incognito --start-maximized 
Hidden=false
X-MATE-Autostart-enabled=true
Name[en_US]=Start from here
Name=Chromium
Comment[en_US]=
Comment=
Icon=/usr/share/icons/LoginIcons/apps/48/view-refresh.svg
Comment[en_US.UTF-8]=Dashboard
EOF
chown student.student /home/student/.config/autostart/virtualta.desktop
chmod 700 /home/student/.config/autostart/virtualta.desktop
cp -rp /home/student/.config/autostart/virtualta.desktop /home/student/Desktop/virtualta.desktop
fi

# Wait for the network to come up

counter=10

while [ $counter -gt 0  ]; do
# Change virtualTA URL according to the location of your TA
curl -H --max-time 5 --connect-timeout 3 "Content-Type: application/json" -X GET https://i-tee.itcollege.ee:8433/api/v1/test/ -d '{"api_key":"usertoken"}' && touch /tmp/pingsuccess && exit 0

sleep 1

# Log to syslog if ping fails

touch /tmp/pingfailed

$((counter--))

done

exit 0
